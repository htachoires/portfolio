<?php


namespace App\Form;


use App\Entity\Experience;
use App\Entity\Person;
use App\Entity\Project;
use App\Entity\Skill;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\Validator\Constraints\NotNull;

class PersonEditType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstName', TextType::class, [
                'label' => 'form.name.first'
            ])
            ->add('lastName', TextType::class, [
                'label' => 'form.name.last'
            ])
            ->add('birth', TextType::class, [
                'mapped' => false,
                'label' => 'form.birth',
                'attr' => [
                    'class' => 'date mb-3',
                ], 'constraints' => [
                    new NotNull()
                ]
            ])
            ->add('description', TextareaType::class, [
                'required' => false,
                'label' => 'form.description.description',
            ])
            ->add('phoneNumber', TelType::class, [
                'label' => 'form.phone_number'
            ])
            ->add('email', EmailType::class, [
                'translation_domain' => false,
            ])
            ->add('password', PasswordType::class, [
                'mapped' => false,
                'required' => true,
                'label' => 'form.password.current',
                'constraints' => [
                    new NotNull()
                ]
            ])
            ->add('picture', FileType::class, [
                'label' => false,
                'mapped' => false,
                'required' => false,
                'attr' => [
                    'placeholder' => 'form.picture.info',
                    'class' => 'text-nowrap',
                ],
                'constraints' => [
                    new File([
                        'maxSize' => '1024k',
                        'mimeTypes' => [
                            'image/jpeg',
                            'image/png',
                        ],
                        'mimeTypesMessage' => 'form.picture.error'
                    ])
                ]
            ])
            ->add('cv', FileType::class, [
                'label' => false,
                'mapped' => false,
                'required' => false,
                'attr' => [
                    'placeholder' => 'CV... (pdf)'
                ],
                'constraints' => [
                    new File([
                        'maxSize' => '8192k',
                        'mimeTypes' => [
                            'application/pdf',
                        ],
                        'mimeTypesMessage' => 'cv.error'
                    ])
                ]
            ])
            ->add('youtube', null, [
                'translation_domain' => false,
            ])
            ->add('linkedin', null, [
                'translation_domain' => false,
            ])
            ->add('github', null, [
                'translation_domain' => false,
            ])
            ->add('skills', EntityType::class, [
                'class' => Skill::class,
                'label' => 'skill.skills',
                'by_reference' => false,
                'required' => false,
                'multiple' => true,
                'attr' => [
                    'class' => 'js-example-basic-single',
                ],
            ])->add('experiences', EntityType::class, [
                'class' => Experience::class,
                'label' => 'experience.experiences',
                'required' => false,
                'by_reference' => false,
                'multiple' => true,
                'attr' => [
                    'class' => 'js-example-basic-single',
                ],
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Person::class,
        ]);
    }
}