<?php

namespace App\Form;

use App\Entity\Skill;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SkillType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $logo = $options['logo'] ?? 'heart';
        $builder
            ->add('logo', TextType::class, [
                'translation_domain' => false,
                'data' => $logo,
            ])
            ->add('name', TextType::class, [
                'label' => 'form.name.name'
            ])
            ->add('description', TextareaType::class, [
                'label' => 'form.description'
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Skill::class,
            'logo' => 'heart'
        ]);
    }
}
