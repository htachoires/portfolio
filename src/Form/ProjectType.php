<?php

namespace App\Form;

use App\Entity\Project;
use App\Entity\Link;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\Validator\Constraints\NotNull;

class ProjectType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'label' => 'form.name.name'
            ])
            ->add('picture', FileType::class, [
                'label' => false,
                'attr' => [
                    'placeholder' => 'form.picture.project.info'
                ],
                'mapped' => false,
                'required' => false,
                'constraints' => [
                    new File([
                        'maxSize' => '1024k',
                        'mimeTypes' => [
                            'image/jpeg',
                            'image/png',
                        ],
                        'mimeTypesMessage' => 'form.picture.project.error'
                    ]),
                ]
            ])
            ->add('summary', TextareaType::class, [
                'label' => 'form.summary'
            ])
            ->add('description', TextareaType::class, [
                'required' => false,
                'label' => 'form.description.description',
                'attr' => [
                    'class' => 'd-none',
                ]
            ])
            ->add('startDate', TextType::class, [
                'mapped' => false,
                'label'=>'date.start',
                'attr' => [
                    'class' => 'date ',
                ], 'constraints' => [
                    new NotNull()
                ],
            ])
            ->add('endDate', TextType::class, [
                'mapped' => false,
                'label'=>'date.end.end',
                'required' => false,
                'attr' => [
                    'placeholder' => 'date.end.empty',
                    'class' => 'date',
                    'value' => '',
                ],
            ])
            ->add('links', EntityType::class, [
                'class' => Link::class,
                'multiple' => true,
                'label'=>'link.links',
                'by_reference' => false,
                'required' => false,
                'attr' => [
                    'class' => 'js-example-basic-single'
                ],
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Project::class,
        ]);
    }
}
