<?php

namespace App\Form;

use App\Entity\Experience;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotNull;

class ExperienceType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, [
                'label' => 'form.title'
            ])
            ->add('label', TextType::class, [
                'label' => 'form.label'
            ])
            ->add('description', TextareaType::class, [
                'label' => 'form.description'
            ])
            ->add('startDate', TextType::class, [
                'mapped' => false,
                'label' => 'date.start',
                'attr' => [
                    'class' => 'date',
                ], 'constraints' => [
                    new NotNull()
                ]
            ])
            ->add('endDate', TextType::class, [
                'mapped' => false,
                'label' => 'date.end',
                'attr' => [
                    'class' => 'date',
                ], 'constraints' => [
                    new NotNull()
                ]
            ])
            ->add('category', ChoiceType::class, [
                'choice_translation_domain' => false,
                'label'=>'form.category',
                'attr' => [
                    'class' => 'w-50 ml-3'
                ],
                'choices' => [
                    0 => 0,
                    1 => 1,
                ]
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Experience::class,
        ]);
    }
}
