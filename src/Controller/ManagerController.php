<?php

namespace App\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/manager")
 * @IsGranted("ROLE_USER")
 */
class ManagerController extends AbstractController
{

    /**
     * @Route("/", name="manager_index", methods={"GET"})
     */
    public function index()
    {
        return $this->render('manager/index.html.twig', [
        ]);
    }

}
