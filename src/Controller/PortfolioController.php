<?php


namespace App\Controller;

use App\Entity\Contact;
use App\Entity\Experience;
use App\Entity\Person;
use App\Entity\Project;
use App\Form\ContactType;
use App\Repository\PersonRepository;
use App\Repository\ProjectRepository;
use App\Repository\ProjectStatisticRepository;
use App\Repository\SkillRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Entity;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @Route("/portfolio")
 */
class PortfolioController extends AbstractController
{

    /**
     * @Route("/{id}", name="portfolio_index", methods={"GET"})
     */
    public function index(Person $person, ProjectRepository $projectRepository, SkillRepository $skillRepository): Response
    {
        return $this->render('portfolio/index.html.twig', [
            'person' => $person,
            'skills' => $skillRepository->findSkills($person, 3, true),
            'projects' => $projectRepository->findVisibleProject($person, 3, true),
            'highlightedProject' => $projectRepository->findVisibleProject($person, 1, true),
            'homeP' => true,
        ]);
    }

    /**
     * @Route("/{id}/cv", name="portfolio_cv", methods={"GET"})
     */
    public function cv(Person $person): Response
    {
        $workExperiences = $this->getDoctrine()->getRepository(Experience::class)->findByCategory(0, $person->getId());
        $educationExperiences = $this->getDoctrine()->getRepository(Experience::class)->findByCategory(1, $person->getId());
        return $this->render('portfolio/cv.html.twig', [
            'person' => $person,
            'workExperiences' => $workExperiences,
            'educationExperiences' => $educationExperiences,
            'currentDate' => new \DateTime(),
            'cvP' => true,
        ]);
    }

    /**
     * @Route("/{id}/projects", name="portfolio_projects", methods={"GET"})
     */
    public function projects(Person $person, ProjectRepository $projectRepository): Response
    {
        return $this->render('portfolio/projects.html.twig', [
            'person' => $person,
            'projects' => $person->getVisibleProjects(),
            'projectsP' => true,
        ]);
    }

    /**
     * @Route("/{person}/project/{project}", name="portfolio_project", methods={"GET"})
     * @Entity("person", expr="repository.find(person)")
     */
    public function project(Project $project, Person $person, SessionInterface $session, ProjectRepository $projectRepository, ProjectStatisticRepository $statisticRepository): Response
    {
        $moreProjects = $projectRepository->findAllProjectsCreatedFollowedWithBlackList($person, $project, 4, true);
        $statisticRepository->incrementViewProject($project);
        $views = $statisticRepository->totalViews([$project]);
        return $this->render('portfolio/project.html.twig', [
            'person' => $person,
            'project' => $project,
            'views' => $views,
            'is_star' => $session->get('star-' . $project->getId(), 'false'),
            'moreProjects' => $moreProjects,
            'currentDate' => new \DateTime(),
        ]);
    }

    /**
     * @Route("/star/{id}", name="portfolio_project_star", methods={"GET"})
     */
    public function star(Project $project, SessionInterface $session): Response
    {
        $response = new Response();

        if ($session->get('star-' . $project->getId(), 'false') === 'false') {
            $session->set('star-' . $project->getId(), 'true');
            $project->incrementStar();
            $session->set('star-' . $project->getId(), 'true');
            $response->setContent(json_encode([
                'response' => 'add',
                'logo' => 'star',
                'title' => 'unstar this project',
                'star' => $project->getStar(),
            ]));
        } else {
            $project->decrementStar();
            $session->set('star-' . $project->getId(), 'false');
            $response->setContent(json_encode([
                'response' => 'del',
                'logo' => 'star-outline',
                'title' => 'star this project',
                'star' => $project->getStar(),
            ]));
        }

        $response->headers->set('Content-Type', 'application/json');
        $this->getDoctrine()->getManager()->flush();
        return $response;
    }

    /**
     * @Route("/contact/{id}", name="portfolio_contact",methods={"GET","POST"})
     */
    public function contact(Person $person, Request $request)
    {
        $contact = new Contact();
        $form = $this->createForm(ContactType::class, $contact);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->addFlash('success', 'Your message has been sent to ' . $person->getEmail() . ' !');
            return $this->redirectToRoute('portfolio_index', [
                'id' => $person->getId()
            ]);
        }

        return $this->render('portfolio/contact.html.twig', [
            'person' => $person,
            'form' => $form->createView(),
        ]);
    }

}