<?php

namespace App\Controller;

use App\Entity\Skill;
use App\Form\SkillType;
use Knp\Component\Pager\PaginatorInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/manager/skill")
 * @IsGranted("ROLE_USER")
 */
class SkillController extends AbstractController
{
    private $type = 'skill';

    /**
     * @Route("/", name="skill_index", methods={"GET"})
     */
    public function index(PaginatorInterface $paginator, Request $request): Response
    {
        $pagination = $paginator->paginate(
            $this->getUser()->getSkills(),
            $request->query->getInt('index', 1),
            8
        );

        return $this->render('manager/entity/skill/index.html.twig', [
            'pagination' => $pagination,
            'type' => $this->type,
        ]);
    }

    /**
     * @Route("/new", name="skill_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $skill = new Skill();
        $form = $this->createForm(SkillType::class, $skill);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $skill->setCreatedBy($this->getUser());
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($skill);
            $entityManager->flush();

            return $this->redirectToRoute('skill_index');
        }

        return $this->render('manager/entity/skill/new.html.twig', [
            'skill' => $skill,
            'type' => $this->type,
            'action' => 'new',
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}/edit", name="skill_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Skill $skill): Response
    {
        if (!$this->getUser()->getSkills()->contains($skill)) {
            return $this->redirectToRoute('skill_index');
        }

        $form = $this->createForm(SkillType::class, $skill, ['logo' => $skill->getLogo()]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('skill_index');
        }

        return $this->render('manager/entity/skill/edit.html.twig', [
            'skill' => $skill,
            'type' => $this->type,
            'action' => 'edit',
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="skill_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Skill $skill): Response
    {
        if (!$this->getUser()->getSkills()->contains($skill)) {
            return $this->redirectToRoute('skill_index');
        }

        if ($this->isCsrfTokenValid('delete' . $skill->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($skill);
            $entityManager->flush();
        }

        return $this->redirectToRoute('skill_index');
    }

    /**
     * @Route("/ajax/{id}", name="skill_delete_ajax", methods={"DELETE"})
     */
    public function deleteAjax(Request $request, Skill $skill): Response
    {
        $response = new Response();
        $response->headers->set('Content-Type', 'application/json');

        if (!$this->getUser()->getSkills()->contains($skill)) {
            $response->setContent(json_encode([
                'error' => 'forbidden',
            ]))
                ->setStatusCode(Response::HTTP_FORBIDDEN);
            return $response;
        }

        if ($this->isCsrfTokenValid('delete' . $skill->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($skill);
            $response->setContent(json_encode([
                'id' => $skill->getId(),
            ]))
                ->setStatusCode(Response::HTTP_OK);
            $entityManager->flush();
            return $response;
        }

        $response->setContent(json_encode([
            'ERROR' => 'invalid values',
        ]))->setStatusCode(Response::HTTP_BAD_REQUEST);

        return $response;
    }

}
