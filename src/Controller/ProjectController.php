<?php

namespace App\Controller;

use App\Entity\Person;
use App\Entity\Project;
use App\Entity\ProjectStatistic;
use App\Entity\Link;
use App\Form\ProjectType;
use App\Repository\ProjectRepository;
use App\Repository\ProjectStatisticRepository;
use Knp\Component\Pager\PaginatorInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\String\Slugger\SluggerInterface;

/**
 * @Route("/manager/project")
 * @IsGranted("ROLE_USER")
 */
class ProjectController extends AbstractController
{
    private $type = 'project';

    /**
     * @Route("/", name="project_index", methods={"GET"})
     */
    public function index(PaginatorInterface $paginator, Request $request, ProjectRepository $projectRepository): Response
    {
        $pagination = $paginator->paginate(
            $projectRepository->findAllProjectsCreatedFollowed($this->getUser()),
            $request->query->getInt('index', 1),
            7
        );

        return $this->render('manager/entity/project/index.html.twig', [
            'pagination' => $pagination,
            'type' => $this->type,
        ]);
    }

    /**
     * @Route("/new", name="project_new", methods={"GET","POST"})
     */
    public function new(Request $request, SluggerInterface $slugger): Response
    {
        $project = new Project();
        $form = $this->createForm(ProjectType::class, $project);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $projectPictureFile = $form->get('picture')->getData();
            // this condition is needed because the 'brochure' field is not required
            // so the PDF file must be processed only when a file is uploaded
            if ($projectPictureFile) {
                $originalFilename = pathinfo($projectPictureFile->getClientOriginalName(), PATHINFO_FILENAME);
                // this is needed to safely include the file name as part of the URL
                $safeFilename = $slugger->slug($originalFilename);
                $newFilename = $safeFilename . '-' . uniqid() . '.' . $projectPictureFile->guessExtension();

                // Move the file to the directory where brochures are stored
                try {
                    $projectPictureFile->move(
                        $this->getParameter('projects_pictures_directory'),
                        $newFilename
                    );
                } catch (FileException $e) {
                    // ... handle exception if something happens during file upload
                }

                // updates the 'brochureFilename' property to store the PDF file name
                // instead of its contents
                $project->setPicture($newFilename);
            }
            $entityManager->getRepository(ProjectStatistic::class)->initStat($project);
            $project->setCreatedBy($this->getUser());
            $entityManager->flush();

            return $this->redirectToRoute('project_index');
        }

        return $this->render('manager/entity/project/new.html.twig', [
            'project' => $project,
            'type' => $this->type,
            'action' => 'new',
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="project_show", methods={"GET"})
     */
    public function show(ProjectStatisticRepository $projectStatisticRepository, Project $project): Response
    {
        if ($this->getUser()->getCreatedProjects()->contains($project) || $this->getUser()->getFollowedProjects()->contains($project)) {
            $statistics = $projectStatisticRepository->findStatistics([$project], 10);
            return $this->render('manager/entity/project/show.html.twig', [
                'project' => $project,
                'statistics' => $statistics[0],
                'type' => $this->type,
                'action' => 'show',
            ]);
        } else {
            return $this->redirectToRoute('project_index');
        }
    }

    /**
     * @Route("/{id}/edit", name="project_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Project $project, SluggerInterface $slugger): Response
    {
        if (!$this->getUser()->getCreatedProjects()->contains($project)) {
            return $this->redirectToRoute('project_index');
        }

        $form = $this->createForm(ProjectType::class, $project);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $projectPictureFile = $form->get('picture')->getData();
            // this condition is needed because the 'brochure' field is not required
            // so the PDF file must be processed only when a file is uploaded
            if ($projectPictureFile) {
                $originalFilename = pathinfo($projectPictureFile->getClientOriginalName(), PATHINFO_FILENAME);
                // this is needed to safely include the file name as part of the URL
                $safeFilename = $slugger->slug($originalFilename);
                $newFilename = $safeFilename . '-' . uniqid() . '.' . $projectPictureFile->guessExtension();

                // Move the file to the directory where brochures are stored
                try {
                    $projectPictureFile->move(
                        $this->getParameter('projects_pictures_directory'),
                        $newFilename
                    );
                } catch (FileException $e) {
                    // ... handle exception if something happens during file upload
                }

                // updates the 'brochureFilename' property to store the PDF file name
                // instead of its contents
                $this->deleteCurrentPicture($project);
                $project->setPicture($newFilename);
            }

            try {
                $project->setStartDate(new \DateTime($form->get('startDate')->getData()));
            } catch (\Exception $e) {
                $form->get('startDate')->addError(new FormError('The start date contains errors'));
                return $this->render('manager/entity/project/edit.html.twig', [
                    'project' => $project,
                    'type' => $this->type,
                    'action' => 'edit',
                    'form' => $form->createView(),
                ]);
            }

            try {
                $project->setEndDate(new \DateTime($form->get('endDate')->getData()));
            } catch (\Exception $e) {
                $form->get('endDate')->addError(new FormError('The end date contains errors'));
                return $this->render('manager/entity/project/edit.html.twig', [
                    'project' => $project,
                    'type' => $this->type,
                    'action' => 'edit',
                    'form' => $form->createView(),
                ]);
            }
            $entityManager->flush();
            return $this->redirectToRoute('project_index');
        }

        return $this->render('manager/entity/project/edit.html.twig', [
            'project' => $project,
            'type' => $this->type,
            'action' => 'edit',
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="project_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Project $project): Response
    {
        if (!$this->getUser()->getCreatedProjects()->contains($project)) {
            return $this->redirectToRoute('project_index');
        }

        if ($this->isCsrfTokenValid('delete' . $project->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($project);
            $this->deleteCurrentPicture($project);
            $entityManager->flush();

        }
        return $this->redirectToRoute('project_index');
    }

    private function deleteCurrentPicture(Project $project)
    {
        try {
            if ($project->getPicture() != $_ENV['DEFAULT_PROJECT_PICTURE'])
                unlink($this->getParameter('projects_pictures_directory') . '/' . $project->getPicture());
        } catch (\Exception $e) {

        }
    }

    /**
     * @Route("/{id}/visible/person/{person}", name="project_visible", methods={"GET"})
     */
    public function visible(Person $person, Project $project): Response
    {
        $response = new Response();
        $response->headers->set('Content-Type', 'application/json');

        if ($person != $this->getUser()) {
            return $response->setContent(json_encode([
                'message' => 'forbidden',
            ]))->setStatusCode(Response::HTTP_FORBIDDEN);
        }

        if ($person->getVisibleProjects()->contains($project)) {
            $person->removeVisibleProject($project);
            $response->setContent(json_encode([
                'message' => 'remove',
                'project' => $project->getId(),
                'logo' => 'eye-off',
            ]))->setStatusCode(Response::HTTP_OK);
        } else {
            $person->addVisibleProject($project);
            $response->setContent(json_encode([
                'message' => 'add',
                'project' => $project->getId(),
                'logo' => 'earth',
            ]))->setStatusCode(Response::HTTP_OK);
        }

        $this->getDoctrine()->getManager()->flush();
        return $response;
    }

    /**
     * @Route("/ajax/{id}", name="project_delete_ajax", methods={"DELETE"})
     */
    public function deleteAjax(Request $request, Project $project): Response
    {
        $response = new Response();
        $response->headers->set('Content-Type', 'application/json');

        if (!$this->getUser()->getCreatedProjects()->contains($project)) {
            $response->setContent(json_encode([
                'error' => 'forbidden',
            ]))
                ->setStatusCode(Response::HTTP_FORBIDDEN);
            return $response;
        }


        if ($this->isCsrfTokenValid('delete' . $project->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($project);
            $response->setContent(json_encode([
                'id' => $project->getId(),
            ]))
                ->setStatusCode(Response::HTTP_OK);
            $entityManager->flush();
            return $response;
        }

        $response->setContent(json_encode([
            'ERROR' => 'invalid values',
        ]))->setStatusCode(Response::HTTP_BAD_REQUEST);

        return $response;
    }
}
