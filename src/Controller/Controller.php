<?php


namespace App\Controller;

use App\Entity\Person;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class Controller extends AbstractController
{
    private $person;

    /**
     * Controller constructor.
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->person = $entityManager->getRepository(Person::class)->findOneBy(['email' => $_ENV['CREATOR']]);
    }

    /**
     * @Route("/", name="index", methods={"GET"})
     */
    public function index(): Response
    {
        return $this->redirectToRoute('portfolio_index', [
            'id' => $this->person->getId()
        ]);
    }

    /**
     * @Route("/projects", name="projects", methods={"GET"})
     */
    public function projects(): Response
    {
        return $this->redirectToRoute('portfolio_projects', [
            'id' => $this->person->getId()
        ]);
    }

    /**
     * @Route("/cv", name="cv", methods={"GET"})
     */
    public function cv(): Response
    {
        return $this->redirectToRoute('portfolio_cv', [
            'id' => $this->person->getId()
        ]);
    }

}