<?php

namespace App\Controller;

use App\Entity\Experience;
use App\Form\ExperienceType;
use App\Repository\ExperienceRepository;
use Knp\Component\Pager\PaginatorInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @Route("/manager/experience")
 * @IsGranted("ROLE_USER")
 */
class ExperienceController extends AbstractController
{

    private $type = 'experience';

    /**
     * @Route("/", name="experience_index", methods={"GET"})
     */
    public function index(ExperienceRepository $experienceRepository, PaginatorInterface $paginator, Request $request): Response
    {
        $pagination = $paginator->paginate(
            $this->getUser()->getExperiences(),
            $request->query->getInt('index', 1),
            7
        );

        return $this->render('manager/entity/experience/index.html.twig', [
            'pagination' => $pagination,
            'type' => $this->type,
        ]);
    }

    /**
     * @Route("/new", name="experience_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $experience = new Experience();
        $form = $this->createForm(ExperienceType::class, $experience);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($experience);

            try {
                $experience->setStartDate(new \DateTime($form->get('startDate')->getData()));
            } catch (\Exception $e) {
                $form->get('startDate')->addError(new FormError('Start date contains errors'));
                return $this->render('manager/entity/experience/edit.html.twig', [
                    'experience' => $experience,
                    'type' => $this->type,
                    'action' => 'new',
                    'form' => $form->createView(),
                ]);
            }

            try {
                $experience->setEndDate(new \DateTime($form->get('endDate')->getData()));
            } catch (\Exception $e) {
                $form->get('endDate')->addError(new FormError('End date contains errors'));
                return $this->render('manager/entity/experience/edit.html.twig', [
                    'experience' => $experience,
                    'type' => $this->type,
                    'action' => 'new',
                    'form' => $form->createView(),
                ]);
            }
            $experience->setCreatedBy($this->getUser());

            $entityManager->flush();

            return $this->redirectToRoute('experience_index');
        }

        return $this->render('manager/entity/experience/new.html.twig', [
            'experience' => $experience,
            'type' => $this->type,
            'action' => 'new',
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}/edit", name="experience_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Experience $experience): Response
    {
        if (!$this->getUser()->getExperiences()->contains($experience)) {
            return $this->redirectToRoute('experience_index');
        }
        $form = $this->createForm(ExperienceType::class, $experience);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $experience->setStartDate(new \DateTime($form->get('startDate')->getData()));
            } catch (\Exception $e) {
                $form->get('startDate')->addError(new FormError('Start date contains errors'));
                return $this->render('manager/entity/experience/edit.html.twig', [
                    'experience' => $experience,
                    'type' => $this->type,
                    'action' => 'edit',
                    'form' => $form->createView(),
                ]);
            }

            try {
                $experience->setEndDate(new \DateTime($form->get('endDate')->getData()));
            } catch (\Exception $e) {
                $form->get('endDate')->addError(new FormError('End date contains errors'));
                return $this->render('manager/entity/experience/edit.html.twig', [
                    'experience' => $experience,
                    'type' => $this->type,
                    'action' => 'edit',
                    'form' => $form->createView(),
                ]);
            }
            $this->getDoctrine()->getManager()->flush();
            return $this->redirectToRoute('experience_index');
        }

        return $this->render('manager/entity/experience/edit.html.twig', [
            'experience' => $experience,
            'type' => $this->type,
            'action' => 'edit',
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="experience_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Experience $experience): Response
    {
        if (!$this->getUser()->getExperiences()->contains($experience)) {
            return $this->redirectToRoute('experience_index');
        }
        if ($this->isCsrfTokenValid('delete' . $experience->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($experience);
            $entityManager->flush();
        }

        return $this->redirectToRoute('experience_index');
    }

    /**
     * @Route("/ajax/{id}", name="experience_delete_ajax", methods={"DELETE"})
     */
    public function deleteAjax(Request $request, Experience $experience): Response
    {
        $response = new Response();
        $response->headers->set('Content-Type', 'application/json');

        if (!$this->getUser()->getExperiences()->contains($experience)) {
            $response->setContent(json_encode([
                'error' => 'forbidden',
            ]))
                ->setStatusCode(Response::HTTP_FORBIDDEN);
            return $response;
        }

        if ($this->isCsrfTokenValid('delete' . $experience->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($experience);
            $response->setContent(json_encode([
                'id' => $experience->getId(),
            ]))
                ->setStatusCode(Response::HTTP_OK);
            $entityManager->flush();
            return $response;
        }

        $response->setContent(json_encode([
            'ERROR' => 'invalid values',
        ]))->setStatusCode(Response::HTTP_BAD_REQUEST);

        return $response;
    }
}
