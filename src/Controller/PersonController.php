<?php

namespace App\Controller;

use App\Entity\Person;
use App\Form\ChangePasswordType;
use App\Form\PersonEditType;
use App\Form\PersonType;
use App\Repository\PersonRepository;
use App\Repository\ProjectRepository;
use App\Repository\ProjectStatisticRepository;
use Knp\Component\Pager\PaginatorInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\String\Slugger\SluggerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @Route("/manager/person")
 * @IsGranted("ROLE_USER")
 */
class PersonController extends AbstractController
{
    private $type = 'person';
    private $personRepository;
    private $projectRepository;
    private $projectStatisticRepository;
    private $slugger;
    private $passwordEncoder;
    private $translator;


    /**
     * PersonController constructor.
     * @param PersonRepository $personRepository
     * @param ProjectRepository $projectRepository
     * @param ProjectStatisticRepository $projectStatisticRepository
     * @param SluggerInterface $slugger
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @param TranslatorInterface $translator
     */
    public function __construct(PersonRepository $personRepository, ProjectRepository $projectRepository,
                                ProjectStatisticRepository $projectStatisticRepository, SluggerInterface $slugger,
                                UserPasswordEncoderInterface $passwordEncoder, TranslatorInterface $translator)
    {
        $this->personRepository = $personRepository;
        $this->projectRepository = $projectRepository;
        $this->projectStatisticRepository = $projectStatisticRepository;
        $this->slugger = $slugger;
        $this->passwordEncoder = $passwordEncoder;
        $this->translator = $translator;
    }


    /**
     * @Route("/", name="person_index", methods={"GET"})
     */
    public function index(PaginatorInterface $paginator, Request $request): Response
    {
        if (!$this->isGranted("ROLE_ADMIN", $this->getUser())) {
            return $this->redirectToRoute('person_show', [
                'id' => $this->getUser()->getId(),
            ]);
        }
        $pagination = $paginator->paginate(
            $this->personRepository->findAll(),
            $request->query->getInt('index', 1),
            7
        );

        return $this->render('manager/entity/person/index.html.twig', [
            'pagination' => $pagination,
            'type' => $this->type,
        ]);
    }

    /**
     * @Route("/new", name="person_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        if (!$this->isGranted("ROLE_ADMIN", $this->getUser())) {
            return $this->redirectToRoute('person_show', [
                'id' => $this->getUser()->getId(),
            ]);
        }

        $person = new Person();
        $form = $this->createForm(PersonType::class, $person);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $person->setPassword($this->passwordEncoder->encodePassword($person, $person->getPassword()));

            $this->saveFile($person,
                $form->get('picture')->getData(),
                $this->slugger,
                $this->getParameter('profiles_pictures_directory'));

            $entityManager->persist($person);
            $entityManager->flush();
            return $this->redirectToRoute('person_index');
        }

        return $this->render('manager/entity/person/new.html.twig', [
            'person' => $person,
            'type' => $this->type,
            'action' => 'new',
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="person_show", methods={"GET"})
     */
    public function show(Person $person): Response
    {
        if ($this->getUser() != $person && !$this->isGranted("ROLE_ADMIN", $this->getUser())) {
            return $this->redirectToRoute('person_index');
        }

        $formChangePassword = null;
        if ($this->getUser() == $person) {
            $formChangePassword = $this->createForm(ChangePasswordType::class)->createView();
        }

        $allProjects = $this->projectRepository->findAllProjectsCreatedFollowed($person);
        $allCreatedProjects = $person->getCreatedProjects();
        $allFollowedProjects = $this->projectRepository->findProjectsNotCreated($person);

        $statisticsProjectsAll = $this->projectStatisticRepository->findStatistics($allProjects, 10);
        $statisticsProjectsCreated = $this->projectStatisticRepository->findStatistics($allCreatedProjects, 10);
        $statisticsProjectsFollowed = $this->projectStatisticRepository->findStatistics($allFollowedProjects, 10);

        $totalViews = $this->projectStatisticRepository->totalViews($allProjects);
        $totalViewsProjectsCreated = $this->projectStatisticRepository->totalViews($allCreatedProjects);
        $totalViewsProjectsFollowed = $this->projectStatisticRepository->totalViews($allFollowedProjects);

        return $this->render('manager/entity/person/show.html.twig', [
            'person' => $person,
            'type' => $this->type,
            'action' => 'show',
            'formChangePassword' => $formChangePassword,
            'statisticsProjectsAll' => $statisticsProjectsAll,
            'statisticsProjectsCreated' => $statisticsProjectsCreated,
            'statisticsProjectsFollowed' => $statisticsProjectsFollowed,
            'totalViewsProjectsAll' => $totalViews,
            'totalViewsProjectsCreated' => $totalViewsProjectsCreated,
            'totalViewsProjectsFollowed' => $totalViewsProjectsFollowed,
            'homeP' => true,
        ]);
    }

    /**
     * @Route("/{id}/edit/password", name="person_edit_password", methods={"POST"})
     */
    public function editPassword(Person $person, Request $request)
    {
        $response = new Response();
        $response->headers->set('Content-Type', 'application/json');

        if ($this->getUser() != $person) {
            return $response->setContent(json_encode([
                'message' => $this->translator->trans('form.error.forbidden')
            ]))->setStatusCode(Response::HTTP_FORBIDDEN);
        }

        $currentPassword = $request->request->get('currentPassword');
        $newPassword_first = $request->request->get('newPassword_first');
        $newPassword_second = $request->request->get('newPassword_second');
        $_token = $request->request->get('_token');

        if ($this->isCsrfTokenValid('edit_password', $_token)) {
            if ($currentPassword == null || $newPassword_first == null || $newPassword_second == null) {
                return $response->setContent(json_encode([
                    'message' =>  $this->translator->trans('form.error.empty')
                ]))->setStatusCode(Response::HTTP_BAD_REQUEST);
            }

            if (!$this->passwordEncoder->isPasswordValid($person, $currentPassword)) {
                return $response->setContent(json_encode([
                    'message' =>  $this->translator->trans('form.error.current_password')
                ]))->setStatusCode(Response::HTTP_BAD_REQUEST);
            }

            if ($newPassword_first != $newPassword_second) {
                return $response->setContent(json_encode([
                    'message' =>  $this->translator->trans('form.error.repeated')
                ]))->setStatusCode(Response::HTTP_BAD_REQUEST);
            }

            $person->setPassword($this->passwordEncoder->encodePassword($person, $newPassword_first));
            $this->getDoctrine()->getManager()->flush();

            return $response->setContent(json_encode([
                'message' =>  $this->translator->trans('form.success.password'),
            ]))->setStatusCode(Response::HTTP_OK);
        }
        return $response->setContent(json_encode([
            'message' =>  $this->translator->trans('form.error.token'),
        ]))->setStatusCode(Response::HTTP_FORBIDDEN);
    }

    /**
     * @Route("/{id}/edit", name="person_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Person $person): Response
    {
        if ($this->getUser() != $person) {
            return $this->redirectToRoute('person_index');
        }

        $form = $this->createForm(PersonEditType::class, $person);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            if (!$this->passwordEncoder->isPasswordValid($person, $request->request->get('person_edit')['password'])) {
                $form->get('password')->addError(new FormError('The password given doesn\'t match with the current password'));
            }

            try {
                $person->setBirth(new \DateTime($form->get('birth')->getData()));
            } catch (\Exception $e) {
                $form->get('birth')->addError(new FormError('The birth date contains errors'));
                return $this->render('manager/entity/person/edit.html.twig', [
                    'person' => $person,
                    'form' => $form->createView(),
                ]);
            }

            $this->saveFile($person,
                $form->get('picture')->getData(),
                $this->getParameter('profiles_pictures_directory'));

            $this->getDoctrine()->getManager()->flush();
            return $this->redirectToRoute('person_index');
        }

        return $this->render('manager/entity/person/edit.html.twig', [
            'person' => $person,
            'type' => $this->type,
            'action' => 'edit',
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="person_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Person $person): Response
    {
        if ($this->getUser() != $person) {
            return $this->redirectToRoute('person_index');
        }

        if ($this->isCsrfTokenValid('delete' . $person->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($person);
            $entityManager->flush();
            $this->deleteFile($person->getPicture(), $this->getParameter('profiles_pictures_directory'));
        }

        return $this->redirectToRoute('person_index');
    }

    private function saveFile(Person $person, ?UploadedFile $uploadedFile, $directory, $cv = false)
    {
        if ($uploadedFile) {
            $originalFilename = pathinfo($uploadedFile->getClientOriginalName(), PATHINFO_FILENAME);

            $safeFilename = $this->slugger->slug($originalFilename);
            $newFilename = $safeFilename . '-' . uniqid() . '.' . $uploadedFile->guessExtension();

            try {
                $uploadedFile->move(
                    $directory,
                    $newFilename
                );
            } catch (FileException $e) {
            }

            if ($cv) {
                $this->deleteFile($person->getCv(), $this->getParameter('cv_directory'));
                $person->setCv($newFilename);
            } else {
                $this->deleteFile($person->getPicture(), $this->getParameter('profiles_pictures_directory'));
                $person->setPicture($newFilename);
            }
        }
    }

    private function deleteFile(string $fileName, string $directory)
    {
        try {
            if ($fileName != $_ENV['DEFAULT_PROFILE_PICTURE']) {
                unlink($directory . '/' . $fileName);
            }
        } catch (\Exception $e) {

        }
    }


}