<?php

namespace App\Controller;

use App\Entity\Link;
use App\Form\LinkType;
use Knp\Component\Pager\PaginatorInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/manager/link")
 * @IsGranted("ROLE_USER")
 */
class LinkController extends AbstractController
{

    private $type = 'link';

    /**
     * @Route("/", name="link_index", methods={"GET"})
     */
    public function index(PaginatorInterface $paginator, Request $request): Response
    {
        $pagination = $paginator->paginate(
            $this->getUser()->getCreatedLinks(),
            $request->query->getInt('index', 1),
            20
        );

        return $this->render('manager/entity/link/index.html.twig', [
            'pagination' => $pagination,
            'type' => $this->type,
        ]);
    }

    /**
     * @Route("/new", name="link_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $link = new Link();
        $form = $this->createForm(LinkType::class, $link);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $link->setCreatedBy($this->getUser());
            $entityManager->persist($link);
            $entityManager->flush();

            return $this->redirectToRoute('link_index');
        }

        return $this->render('manager/entity/link/new.html.twig', [
            'link' => $link,
            'type' => $this->type,
            'action' => 'new',
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}/edit", name="link_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Link $link): Response
    {
        if (!$this->getUser()->getCreatedLinks()->contains($link)) {
            return $this->redirectToRoute('link_index');
        }

        $form = $this->createForm(LinkType::class, $link);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('link_index');
        }

        return $this->render('manager/entity/link/edit.html.twig', [
            'link' => $link,
            'type' => $this->type,
            'action' => 'edit',
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="link_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Link $link): Response
    {
        if (!$this->getUser()->getCreatedLinks()->contains($link)) {
            return $this->redirectToRoute('link_index');
        }

        if ($this->isCsrfTokenValid('delete' . $link->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($link);
            $entityManager->flush();
        }

        return $this->redirectToRoute('link_index');
    }

    /**
     * @Route("/ajax/{id}", name="link_delete_ajax", methods={"DELETE"})
     */
    public function deleteAjax(Request $request, Link $link): Response
    {
        $response = new Response();
        $response->headers->set('Content-Type', 'application/json');

        if (!$this->getUser()->getCreatedLinks()->contains($link)) {
            $response->setContent(json_encode([
                'error' => 'forbidden',
            ]))
                ->setStatusCode(Response::HTTP_FORBIDDEN);
            return $response;
        }

        if ($this->isCsrfTokenValid('delete' . $link->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($link);
            $response->setContent(json_encode([
                'id' => $link->getId(),
            ]))
                ->setStatusCode(Response::HTTP_OK);
            $entityManager->flush();
            return $response;
        }

        $response->setContent(json_encode([
            'ERROR' => 'invalid values',
        ]))->setStatusCode(Response::HTTP_BAD_REQUEST);

        return $response;
    }
}
