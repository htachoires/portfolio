<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PersonRepository")
 * @UniqueEntity("email")
 */
class Person implements UserInterface
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=false)
     * @Assert\NotBlank()
     * @Assert\Length(max="255")
     */
    private $firstName;

    /**
     * @ORM\Column(type="string", length=255, nullable=false)
     * @Assert\NotBlank()
     * @Assert\Length(max="255")
     */
    private $lastName;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Assert\NotNull()
     */
    private $birth;

    /**
     * @ORM\Column(type="string", length=65556, nullable=true)
     * @Assert\Length(max="65556")
     */
    private $description;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\Regex(pattern="/^[0-9 ]*$/",message="This value can only contain digits")
     * @Assert\Length(max="255")
     */
    private $phoneNumber;

    /**
     * @ORM\Column(type="string", length=255, unique=true, nullable=false)
     * @Assert\Email()
     * @Assert\NotBlank()
     * @Assert\Length(max="255")
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=255, nullable=false)
     * @Assert\NotBlank()
     * @Assert\Length(max="255")
     */
    private $password;

    /**
     * @ORM\Column(type="string", length=1000, nullable=true)
     * @Assert\Length(max="1000")
     */
    private $picture;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\Length(max="255")
     */
    private $cv;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\Length(max="255")
     * @Assert\Regex(pattern="/^https:\/\/youtube.com\/channel\/.*$/")
     */
    private $youtube;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\Length(max="255")
     * @Assert\Regex(pattern="/^https:\/\/linkedin.com\/in\/.*$/")
     */
    private $linkedin;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\Length(max="255")
     * @Assert\Regex(pattern="/^https:\/\/github.com\/.*$/")
     */
    private $github;

    /**
     * @ORM\OneToMany(targetEntity="Skill", mappedBy="createdBy")
     */
    private $skills;

    /**
     * @ORM\OneToMany(targetEntity="Experience", mappedBy="createdBy")
     */
    private $experiences;

    /**
     * @ORM\ManyToMany(targetEntity="Project", mappedBy="followers")
     */
    private $followedProjects;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Project", inversedBy="visibleBy")
     */
    private $visibleProjects;

    /**
     * @ORM\OneToMany(targetEntity="Project", mappedBy="createdBy")
     */
    private $createdProjects;

    /**
     * @ORM\OneToMany(targetEntity="Link", mappedBy="createdBy")
     */
    private $createdLinks;

    /**
     * @ORM\Column(type="boolean", nullable=false)
     * @Assert\NotNull()
     */
    private $isAdmin;




    public function __construct()
    {
        $this->setBirth(new \DateTime("now"));
        $this->followedProjects = new ArrayCollection();
        $this->setIsAdmin(false);
        $this->skills = new ArrayCollection();
        $this->experiences = new ArrayCollection();
        $this->createdProjects = new ArrayCollection();
        $this->createdLinks = new ArrayCollection();
        $this->visibleProjects = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(string $firstName): self
    {
        $this->firstName = ucfirst($firstName);

        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName(string $lastName): self
    {
        $this->lastName = ucfirst($lastName);

        return $this;
    }

    public function getBirth(): ?\DateTimeInterface
    {
        return $this->birth;
    }

    public function setBirth(\DateTimeInterface $birth): self
    {
        $this->birth = $birth;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getPhoneNumber(): ?string
    {
        return $this->phoneNumber;
    }

    public function setPhoneNumber(string $phoneNumber): self
    {
        $this->phoneNumber = $phoneNumber;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getPicture()
    {
        return ($this->picture ?? $_ENV['DEFAULT_PROFILE_PICTURE']);
    }

    public function setPicture($picture): self
    {
        $this->picture = $picture;

        return $this;
    }

    public function getYoutube(): ?string
    {
        return $this->youtube;
    }

    public function setYoutube(?string $youtube): self
    {
        $this->youtube = $youtube;

        return $this;
    }

    public function getLinkedin(): ?string
    {
        return $this->linkedin;
    }

    public function setLinkedin(?string $linkedin): self
    {
        $this->linkedin = $linkedin;

        return $this;
    }

    public function getGithub(): ?string
    {
        return $this->github;
    }

    public function setGithub(?string $github): self
    {
        $this->github = $github;

        return $this;
    }

    /**
     * @return Collection|Project[]
     */
    public function getFollowedProjects(): Collection
    {
        return $this->followedProjects;
    }

    public function addFollowedProject(Project $project): self
    {
        if (!$this->followedProjects->contains($project)) {
            $this->followedProjects[] = $project;
            $project->addFollower($this);
        }

        return $this;
    }

    public function removeFollowedProject(Project $project): self
    {
        if ($this->followedProjects->contains($project)) {
            $this->followedProjects->removeElement($project);
            $project->removeFollower($this);
        }

        return $this;
    }


    /**
     * @return Collection|Skill[]
     */
    public function getSkills(): Collection
    {
        return $this->skills;
    }

    public function addSkill(Skill $skill): self
    {
        if (!$this->skills->contains($skill)) {
            $this->skills[] = $skill;
            $skill->setCreatedBy($this);
        }

        return $this;
    }

    public function removeSkill(Skill $skill): self
    {
        if ($this->skills->contains($skill)) {
            $this->skills->removeElement($skill);
            // set the owning side to null (unless already changed)
            if ($skill->getCreatedBy() === $this) {
                $skill->setCreatedBy(null);
            }
        }

        return $this;
    }

    public function __toString()
    {
        return $this->getEmail();
    }

    /**
     * @return Collection|Experience[]
     */
    public function getExperiences(): Collection
    {
        return $this->experiences;
    }

    public function addExperience(Experience $experience): self
    {
        if (!$this->experiences->contains($experience)) {
            $this->experiences[] = $experience;
            $experience->setCreatedBy($this);
        }

        return $this;
    }

    public function removeExperience(Experience $experience): self
    {
        if ($this->experiences->contains($experience)) {
            $this->experiences->removeElement($experience);
            // set the owning side to null (unless already changed)
            if ($experience->getCreatedBy() === $this) {
                $experience->setCreatedBy(null);
            }
        }

        return $this;
    }


    /**
     * @inheritDoc
     */
    public function getUsername()
    {
        return $this->getEmail();
    }

    /**
     * @inheritDoc
     */
    public function setPassword(?string $password): self
    {
        $this->password = $password;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @inheritDoc
     */
    public function getRoles()
    {
        if ($this->getIsAdmin())
            return ["ROLE_USER", "ROLE_ADMIN"];
        else
            return ["ROLE_USER"];
    }

    /**
     * @inheritDoc
     */
    public function eraseCredentials()
    {
        // TODO: Implement eraseCredentials() method.
    }

    /**
     * @inheritDoc
     */
    public function getSalt()
    {
        // TODO: Implement getSalt() method.
    }

    public function getNbProjectsCreatedAndFollowed(): int
    {
        return $this->getFollowedProjects()->count() + $this->getCreatedProjects()->count();
    }

    /**
     * @return Collection|Project[]
     */
    public function getCreatedProjects(): Collection
    {
        return $this->createdProjects;
    }

    public function addCreatedProject(Project $createdProject): self
    {
        if (!$this->createdProjects->contains($createdProject)) {
            $this->createdProjects[] = $createdProject;
            $createdProject->setCreatedBy($this);
        }

        return $this;
    }

    public function removeCreatedProject(Project $createdProject): self
    {
        if ($this->createdProjects->contains($createdProject)) {
            $this->createdProjects->removeElement($createdProject);
            // set the owning side to null (unless already changed)
            if ($createdProject->getCreatedBy() === $this) {
                $createdProject->setCreatedBy(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Link[]
     */
    public function getCreatedLinks(): Collection
    {
        return $this->createdLinks;
    }

    public function addCreatedLink(Link $createdLink): self
    {
        if (!$this->createdLinks->contains($createdLink)) {
            $this->createdLinks[] = $createdLink;
            $createdLink->setCreatedBy($this);
        }

        return $this;
    }

    public function removeCreatedLink(Link $createdLink): self
    {
        if ($this->createdLinks->contains($createdLink)) {
            $this->createdLinks->removeElement($createdLink);
            // set the owning side to null (unless already changed)
            if ($createdLink->getCreatedBy() === $this) {
                $createdLink->setCreatedBy(null);
            }
        }

        return $this;
    }

    public function getIsAdmin(): ?bool
    {
        return $this->isAdmin;
    }

    public function setIsAdmin(bool $isAdmin): self
    {
        $this->isAdmin = $isAdmin;

        return $this;
    }

    public function getCv(): ?string
    {
        return $this->cv;
    }

    public function setCv(string $cv): self
    {
        $this->cv = $cv;

        return $this;
    }

    /**
     * @return Collection|Project[]
     */
    public function getVisibleProjects(): Collection
    {
        return $this->visibleProjects;
    }

    public function addVisibleProject(Project $visibleProject): self
    {
        if (!$this->visibleProjects->contains($visibleProject)) {
            $this->visibleProjects[] = $visibleProject;
        }

        return $this;
    }

    public function removeVisibleProject(Project $visibleProject): self
    {
        if ($this->visibleProjects->contains($visibleProject)) {
            $this->visibleProjects->removeElement($visibleProject);
        }

        return $this;
    }

}
