<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ProjectRepository")
 */
class Project
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=false)
     * @Assert\NotBlank()
     * @Assert\Length(max="255")
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255, nullable=false)
     * @Assert\NotBlank()
     * @Assert\Length(max="255")
     */
    private $summary;

    /**
     * @ORM\Column(type="string", length=65556, nullable=false)
     * @Assert\NotBlank()
     * @Assert\Length(max="65556")
     */
    private $description;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\Length(max="255")
     */
    private $picture;

    /**
     * @ORM\Column(type="datetime", nullable=false)
     * @Assert\NotNull()
     */
    private $startDate;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $endDate;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Person", inversedBy="createdProjects")
     * @ORM\JoinColumn(name="created_by_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $createdBy;

    /**
     * @ORM\ManyToMany(targetEntity="Person", inversedBy="followedProjects")
     */
    private $followers;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Person", mappedBy="visibleProjects")
     */
    private $visibleBy;

    /**
     * @ORM\ManyToMany(targetEntity="Link", mappedBy="projects")
     */
    private $links;

    /**
     * @ORM\Column(type="string", length=255, nullable=false)
     * @Assert\NotBlank()
     * @Assert\Length(max="255")
     */
    private $star;

    /**
     * @ORM\OneToMany(targetEntity="ProjectStatistic", mappedBy="project")
     */
    private $statistics;



    /**
     * Project constructor.
     */
    public function __construct()
    {
        $this->setStartDate(new \DateTime("now"));
        $this->followers = new ArrayCollection();
        $this->statistics = new ArrayCollection();
        $this->links = new ArrayCollection();
        $this->setStar(0);
        $this->visibleBy = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = ucfirst($name);

        return $this;
    }

    public function getPicture(): ?string
    {
        return ($this->picture ?? $_ENV['DEFAULT_PROJECT_PICTURE']);
    }

    public function setPicture(?string $picture): self
    {
        $this->picture = $picture;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getSummary(): ?string
    {
        return $this->summary;
    }

    public function setSummary(string $summary): self
    {
        $this->summary = ucfirst($summary);

        return $this;
    }

    public function getStartDate(): ?\DateTimeInterface
    {
        return $this->startDate;
    }

    public function setStartDate(\DateTimeInterface $startDate): self
    {
        $this->startDate = $startDate;

        return $this;
    }

    public function getEndDate(): ?\DateTimeInterface
    {
        return $this->endDate;
    }

    public function setEndDate(?\DateTimeInterface $endDate): self
    {
        $this->endDate = $endDate;

        return $this;
    }

    /**
     * @return Collection|Person[]
     */
    public function getFollowers(): Collection
    {
        return $this->followers;
    }

    public function addFollower(Person $person): self
    {
        if (!$this->followers->contains($person)) {
            $this->followers[] = $person;
            $person->addFollowedProject($this);
        }

        return $this;
    }

    public function removeFollower(Person $person): self
    {
        if ($this->followers->contains($person)) {
            $this->followers->removeElement($person);
            $person->removeFollowedProject($this);
        }

        return $this;
    }

    /**
     * @return Collection|Link[]
     */
    public function getLinks(): Collection
    {
        return $this->links;
    }

    public function addLink(Link $link): self
    {
        if (!$this->links->contains($link)) {
            $this->links[] = $link;
            $link->addProject($this);
        }

        return $this;
    }

    public function removeLink(Link $link): self
    {
        if ($this->links->contains($link)) {
            $this->links->removeElement($link);
            $link->removeProject($this);
        }

        return $this;
    }

    public function getStar(): ?string
    {
        return $this->star;
    }

    public function setStar(string $star): self
    {
        $this->star = $star;

        return $this;
    }

    /**
     * @return Collection|self[]
     */
    public function getStatistics(): Collection
    {
        return $this->statistics;
    }

    public function addProjectStatistic(ProjectStatistic $projectStatistic): self
    {
        if (!$this->statistics->contains($projectStatistic)) {
            $this->statistics[] = $projectStatistic;
            $projectStatistic->addProject($this);
        }

        return $this;
    }

    public function removeProjectStatistic(ProjectStatistic $projectStatistic): self
    {
        if ($this->statistics->contains($projectStatistic)) {
            $this->statistics->removeElement($projectStatistic);
            $projectStatistic->removeProject($this);
        }

        return $this;
    }

    public function __toString()
    {
        return $this->getName();
    }

    public function incrementStar()
    {
        $this->setStar($this->getStar() + 1);
    }

    public function decrementStar()
    {
        $this->setStar($this->getStar() - 1);
    }

    public function getCreatedBy(): ?Person
    {
        return $this->createdBy;
    }

    public function setCreatedBy(?Person $createdBy): self
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * @return Collection|Person[]
     */
    public function getVisibleBy(): Collection
    {
        return $this->visibleBy;
    }

    public function addVisibleBy(Person $visibleBy): self
    {
        if (!$this->visibleBy->contains($visibleBy)) {
            $this->visibleBy[] = $visibleBy;
            $visibleBy->addVisibleProject($this);
        }

        return $this;
    }

    public function removeVisibleBy(Person $visibleBy): self
    {
        if ($this->visibleBy->contains($visibleBy)) {
            $this->visibleBy->removeElement($visibleBy);
            $visibleBy->removeVisibleProject($this);
        }

        return $this;
    }

}
