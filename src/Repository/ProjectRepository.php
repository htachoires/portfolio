<?php

namespace App\Repository;

use App\Entity\Person;
use App\Entity\Project;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Project|null find($id, $lockMode = null, $lockVersion = null)
 * @method Project|null findOneBy(array $criteria, array $orderBy = null)
 * @method Project[]    findAll()
 * @method Project[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProjectRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Project::class);
    }

    public function findByPersonRandom($limit, $id)
    {
        return $this->createQueryBuilder('project')
            ->join('project.people', 'people')
            ->where('people.id = :personId')

            ->setParameter('personId', $id)
            ->addSelect('project')
            ->orderBy('project.id','ASC')
            ->setMaxResults($limit)
            ->getQuery()->execute();
    }

    public function findVisibleProject(Person $person, int $limit = -1, bool $random = false)
    {
        $qb = $this->createQueryBuilder('project')
            ->where(':person MEMBER OF project.visibleBy')
            ->setParameter('person', $person);

        if ($limit != -1) $qb->setMaxResults($limit);
        if ($random) $qb->orderBy('project.id','ASC');

        return $qb->getQuery()->execute();
    }

    public function findAllProjectsCreatedFollowedWithBlackList($person, $blacklistedProject, $limit = 0, $random = false)
    {
        $qb = $this->createQueryBuilder('project')
            ->andWhere('project.createdBy = :person')
            ->orWhere(':person MEMBER OF project.followers')
            ->andWhere(':blacklistedProject != project')
            ->setParameters([
                'person' => $person,
                'blacklistedProject' => $blacklistedProject,
            ]);

        if ($random) $qb->addSelect('project')->orderBy('project.id','ASC');
        if ($limit > 0) $qb->setMaxResults($limit);

        return $qb->distinct()
            ->getQuery()
            ->getResult();
    }

    /**
     * @param Person $person
     * @return Project[]
     */
    public function findAllProjectsCreatedFollowed($person, $limit = 0, $random = false)
    {
        $qb = $this->createQueryBuilder('project')
            ->andWhere('project.createdBy = :person')
            ->orWhere(':person MEMBER OF project.followers')
            ->orderBy('project.createdBy', 'DESC')
            ->setParameter('person', $person);

        if ($random) $qb->addSelect('project')->orderBy('project.id','ASC');
        if ($limit > 0) $qb->setMaxResults($limit);

        return $qb->distinct()
            ->getQuery()
            ->getResult();
    }

    public function findProjectsNotCreated(Person $person)
    {
        return $this->createQueryBuilder('project')
            ->andWhere('project.createdBy != :person')
            ->andWhere(':person MEMBER OF project.followers')
            ->setParameter('person', $person)
            ->distinct()
            ->getQuery()
            ->getResult();
    }

}
