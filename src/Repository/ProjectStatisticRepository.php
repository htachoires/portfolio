<?php

namespace App\Repository;

use App\Entity\Project;
use App\Entity\ProjectStatistic;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ProjectStatistic|null find($id, $lockMode = null, $lockVersion = null)
 * @method ProjectStatistic|null findOneBy(array $criteria, array $orderBy = null)
 * @method ProjectStatistic[]    findAll()
 * @method ProjectStatistic[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProjectStatisticRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ProjectStatistic::class);
    }

    public function incrementViewProject(Project $project)
    {
        $stat = $this->findOneBy(['project' => $project->getId()], ['date' => 'DESC']);
        if ($stat->getDate() < new \DateTime("-1 month")) {
            $newStat = new ProjectStatistic();
            $newStat->setValue(1)
                ->setProject($project)
                ->setDate(new \DateTime("now"));
            $this->getEntityManager()->persist($newStat);
        } else {
            $stat->incrementValue();
        }
        $this->getEntityManager()->flush();
    }

    public function initStat(Project $project)
    {
        for ($i = 0; $i < 7; $i++) {
            $newStat = new ProjectStatistic();
            $newStat->setValue(0)
                ->setProject($project)
                ->setDate(new \DateTime(-$i . " months"));
            $this->getEntityManager()->persist($newStat);
        }
        $this->getEntityManager()->flush();
    }

    /**
     * @param Project[] $projects
     * @param $limit
     * @return array
     */
    public function findStatistics($projects, $limit)
    {
        $array = [];
        foreach ($projects as $project) {
            $array[] = array_reverse($this->findBy(['project' => $project->getId()], ['date' => 'DESC'], $limit));
        }
        return $array;
    }

    /**
     * @param Project[] $projects
     * @return int number of views
     */
    public function totalViews($projects): int
    {
        $res = 0;
        foreach ($projects as $project) {
            $statistics = $this->getEntityManager()->getRepository(ProjectStatistic::class)->findBy(['project' => $project->getId()]);
            foreach ($statistics as $statistic) {
                $res += (int)$statistic->getValue();
            }
        }
        return $res;
    }
}
