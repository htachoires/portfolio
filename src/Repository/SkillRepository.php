<?php

namespace App\Repository;

use App\Entity\Person;
use App\Entity\Skill;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Skill|null find($id, $lockMode = null, $lockVersion = null)
 * @method Skill|null findOneBy(array $criteria, array $orderBy = null)
 * @method Skill[]    findAll()
 * @method Skill[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SkillRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Skill::class);
    }

    public function findSkills(Person $person, int $limit = 0, bool $random = false)
    {
        $qb = $this->createQueryBuilder('skill')
            ->andWhere('skill.createdBy = :person')
            ->setParameter('person', $person);

        if ($random) $qb->addSelect('skill')->orderBy('skill.id','ASC');
        if ($limit > 0) $qb->setMaxResults($limit);

        return $qb->distinct()
            ->getQuery()
            ->getResult();
    }

}
