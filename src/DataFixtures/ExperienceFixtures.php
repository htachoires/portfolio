<?php

namespace App\DataFixtures;

use App\Entity\Experience;
use App\Entity\Person;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class ExperienceFixtures extends Fixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $nbExpPerPerson = 10;
        $faker = Factory::create();

        foreach ($manager->getRepository(Person::class)->findAll() as $person) {
            for ($i = 0; $i < $nbExpPerPerson; $i++) {
                $experience = new Experience();
                $experience->setTitle($faker->jobTitle)
                    ->setLabel($faker->city)
                    ->setDescription($faker->sentence(20))
                    ->setCategory(random_int(0, 1))
                    ->setStartDate($faker->dateTimeBetween('-3 years', '-1 year'))
                    ->setEndDate($faker->dateTimeBetween('-1 year', '1 year'))
                    ->setCreatedBy($person);
                $manager->persist($experience);
            }
        }
        $manager->flush();
    }

    /**
     * @inheritDoc
     */
    public function getOrder()
    {
        return 2;
    }
}
