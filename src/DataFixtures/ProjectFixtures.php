<?php

namespace App\DataFixtures;

use App\Entity\Person;
use App\Entity\Project;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use joshtronic\LoremIpsum;

class ProjectFixtures extends Fixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $nbProjectPerPerson = 10;
        $lorem = new LoremIpsum();
        $faker = Factory::create();
        foreach ($manager->getRepository(Person::class)->findAll() as $person) {
            for ($i = 0; $i < $nbProjectPerPerson; $i++) {
                $project = new Project();
                $project->setName($faker->sentence(3))
                    ->setSummary($faker->sentence(15))
                    ->setDescription($lorem->paragraphs(2, 'p'))
                    ->setStartDate($faker->dateTimeBetween('-3 years', '+ 1 month'))
                    ->setEndDate($faker->dateTimeBetween('-3 years', '+ 1 month'))
                    ->setStar(random_int(0, 12))
                    ->setCreatedBy($person);
                $manager->persist($project);
            }
        }
        $manager->flush();
    }

    /**
     * @inheritDoc
     */
    public function getOrder()
    {
        return 4;
    }
}
