<?php

namespace App\DataFixtures;

use App\Entity\Person;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class PersonFixtures extends Fixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $numberPerson = 5;
        $faker = Factory::create();
        for ($i = 0; $i < $numberPerson; $i++) {
            $person = new Person();
            $person->setFirstName($faker->firstName)
                ->setLastName($faker->lastName)
                ->setEmail($faker->email)
                ->setPassword(password_hash("plop", PASSWORD_BCRYPT))
                ->setPhoneNumber($faker->phoneNumber)
                ->setBirth($faker->dateTimeBetween('-30 years', '- 18 years'))
                ->setDescription($faker->sentence(25))
                ->setLinkedin('https://linkedin.com/in/' . $person->getLastName())
                ->setYoutube('https://youtube.com/channel/' . $person->getLastName())
                ->setGithub('https://github.com/' . $person->getLastName());
            $manager->persist($person);
        }

        $admin = new Person();
        $admin->setFirstName("hugo")
            ->setLastName("tachoires")
            ->setEmail($_ENV['CREATOR'])
            ->setIsAdmin(true)
            ->setPassword(password_hash("plop", PASSWORD_BCRYPT))
            ->setPhoneNumber("0696969696")
            ->setBirth($faker->dateTimeBetween('-3 years', '+ 1 month'))
            ->setDescription($faker->sentence(25))
            ->setLinkedin('https://linkedin.com/in/' . $faker->slug(1))
            ->setYoutube('https://youtube.com/channel/' . $faker->slug(1))
            ->setGithub('https://github.com/' . $faker->slug(1));
        $manager->persist($admin);

        $manager->flush();
    }

    /**
     * @inheritDoc
     */
    public function getOrder()
    {
        return 1;
    }
}
