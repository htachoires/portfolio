<?php

namespace App\DataFixtures;

use App\Entity\Project;
use App\Entity\Link;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class TagFixtures extends Fixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $nbTagPerProject = 4;
        $faker = Factory::create();
        foreach ($manager->getRepository(Project::class)->findAll() as $project) {
            for ($i = 0; $i < $nbTagPerProject; $i++) {
                $tag = new Link();
                $tag->setName($faker->slug(2))
                    ->setLabel($faker->sentence(2))
                    ->setUrlResource($faker->url)
                    ->setCreatedBy($project->getCreatedBy());
                $project->addLink($tag);
                $manager->persist($tag);
            }
        }
        $manager->flush();
    }

    /**
     * @inheritDoc
     */
    public function getOrder()
    {
        return 5;
    }
}
