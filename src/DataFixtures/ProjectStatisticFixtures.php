<?php

namespace App\DataFixtures;

use App\Entity\Project;
use App\Entity\ProjectStatistic;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class ProjectStatisticFixtures extends Fixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $numberStatProject = 10;
        foreach ($manager->getRepository(Project::class)->findAll() as $project)
            for ($i = 0; $i < $numberStatProject; $i++) {
                $stat = new ProjectStatistic();
                $stat->setDate(new \DateTime('-' . ($i) . ' months'))
                    ->setValue(random_int(0, 10))
                    ->setProject($project);
                $manager->persist($stat);
            }
        $manager->flush();
    }

    /**
     * @inheritDoc
     */
    public function getOrder()
    {
        return 6;
    }
}
