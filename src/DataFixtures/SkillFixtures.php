<?php

namespace App\DataFixtures;

use App\Entity\Person;
use App\Entity\Skill;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class SkillFixtures extends Fixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $nbSkillPerPerson = 30;
        $faker = Factory::create();

        foreach ($manager->getRepository(Person::class)->findAll() as $person) {
            for ($i = 0; $i < $nbSkillPerPerson; $i++) {
                $skill = new Skill();
                $skill->setName($faker->sentence(2))
                    ->setDescription($faker->sentence(20))
                    ->setLogo('person')
                    ->setCreatedBy($person);
                $manager->persist($skill);
            }
        }
        $manager->flush();
    }

    /**
     * @inheritDoc
     */
    public function getOrder()
    {
        return 3;
    }
}
